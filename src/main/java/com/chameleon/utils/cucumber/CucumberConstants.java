package com.chameleon.utils.cucumber;

import com.chameleon.utils.Constants;

public class CucumberConstants {
    /*
     * public static final String RUNNER_RESULTS_LOCATION = "src/main/resources/json/cucumber-parallel"; <-- used for internal testing
     */

    public static final String RUNNER_RESULTS_LOCATION = "target/cucumber-parallel";
    public static final String REPORT_TEMPLATE_LOCATION = "src/main/resources/html/cucumberReport";
    public static final String REPORT_OUTPUT_LOCATION = Constants.TEST_OUTPUT_FOLDER + Constants.DIR_SEPARATOR + "cucumberReport" + Constants.DIR_SEPARATOR;
    public static final String FEATURE_TAG = "US";
    public static final String SCENARIO_TAG = "TC";

    // Expected POM.xml override tags
    public static final String POM_CUCUMBER_OPTION_REPORT_NAME = "cucumber.report.name";
    public static final String POM_CUCUMBER_OPTION_TIMESTAMP_FORMAT = "cucumber.report.timestamp.format";
    public static final String POM_CUCUMBER_OPTION_TIMESTAMP_ZONE = "cucumber.report.timestamp.zone";
    public static final String POM_CUCUMBER_OPTION_EXECUTION_SUMMARY_ENABLED = "cucumber.report.executionSummary.enabled";
    public static final String POM_CUCUMBER_OPTION_BREAKDOWN_SUMMARY_ENABLED = "cucumber.report.breakdownSummary.enabled";
    public static final String POM_CUCUMBER_OPTION_LONGEST_DURATION_SUMMARY_ENABLED = "cucumber.report.longestDurationSummary.enabled";
    public static final String POM_CUCUMBER_OPTION_TEST_DETAILS_SUAMMRY_ENABLED = "cucumber.report.testDetailsSummary.enabled";
    public static final String POM_CUCUMBER_OPTION_COMMON_FAILING_REASONS_SUMMARY_ENABLED = "cucumber.report.commonFailingReasons.enabled";
    public static final String POM_CUCUMBER_OPTION_GENERATE_PDF = "cucumber.report.generate.pdf.enabled";

    // Report defaults
    public static final String DEFAULT_REPORT_NAME = "Chameleon";
    public static final String DEFAULT_REPORT_TIMESTAMP_FORMAT = "yyyy-MM-dd HH.mm.ss z";
    public static final String DEFAULT_REPORT_TIMEZONE = "America/New_York";
    public static final boolean DEFAULT_REPORT_GENERATE_PDF = false;
    public static final boolean DEFAULT_REPORT_EXECUTION_SUMMARY_ENABLED = true;
    public static final boolean DEFAULT_BREAKDOWN_SUMMARY_ENABLED = true;
    public static final boolean DEFAULT_LONGEST_DURATION_SUMMARY_ENABLED = false;
    public static final boolean DEFAULT_REPORT_TEST_DETAILS_SUMMARY_ENABLED = true;
    public static final boolean DEFAULT_COMMON_FAILING_REASONS_SUMMARY_ENABLED = true;
}
