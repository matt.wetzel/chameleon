package com.chameleon.utils.io;

import java.util.Properties;

public class CaselessProperties extends Properties {
    private static final long serialVersionUID = -3713936963486995273L;

    @Override
    public Object put(final Object key, final Object value) {
        final String lowercase = ((String) key).toLowerCase().trim();
        return super.put(lowercase, value);
    }

    @Override
    public String getProperty(final String key) {
        final String lowercase = key.toLowerCase().trim();
        return super.getProperty(lowercase).trim();
    }

    @Override
    public String getProperty(final String key, final String defaultValue) {
        final String lowercase = key.toLowerCase().trim();
        return super.getProperty(lowercase, defaultValue).trim();
    }
}
