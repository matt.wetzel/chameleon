package com.chameleon.selenium.mobile;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.remote.CapabilityType;

import com.chameleon.AutomationException;
import com.chameleon.BaseTest;
import com.chameleon.Beta;
import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.DriverManagerFactory;
import com.chameleon.selenium.DriverOptionsManager;
import com.chameleon.selenium.DriverProperties;
import com.chameleon.selenium.DriverType;
import com.chameleon.selenium.web.WebException;
import com.chameleon.utils.EncryptDecryptHelper;
import com.saucelabs.common.SauceOnDemandAuthentication;

import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobilePlatform;

/**
 * This class is incomplete
 * 
 * @author justin
 *
 */
@Beta
public class MobileBaseTest extends BaseTest {

    protected static ThreadLocal<String> deviceID = new ThreadLocal<>();
    protected static ThreadLocal<String> mobileOSVersion = new ThreadLocal<>();

    protected static ThreadLocal<String> applicationUnderTest = new ThreadLocal<>();
    protected static ThreadLocal<String> browserUnderTest = new ThreadLocal<>();
    protected static ThreadLocal<String> browserVersion = new ThreadLocal<>();
    protected static ThreadLocal<String> operatingSystem = new ThreadLocal<>();
    protected static ThreadLocal<String> runLocation = new ThreadLocal<>();
    protected static ThreadLocal<String> pageUrl = new ThreadLocal<>();

    /*
     * WebDriver Fields
     */
    protected static ThreadLocal<String> sessionId = new ThreadLocal<>();

    /*
     * Getters and setters
     */
    public void setApplicationUnderTest(String aut) {
        applicationUnderTest.set(aut);
    }

    public String getApplicationUnderTest() {
        return applicationUnderTest.get();
    }

    public void setPageURL(String url) {
        pageUrl.set(url);
    }

    public String getPageURL() {
        return pageUrl.get();
    }

    public String getOperatingSystem() {
        return operatingSystem.get() == null ? "" : operatingSystem.get();
    }

    public void setBrowserUnderTest(String but) {
        if (but.equalsIgnoreCase("jenkinsParameter")) {
            browserUnderTest.set(System.getProperty("jenkinsBrowser").trim());
        } else {
            browserUnderTest.set(but);
        }
    }

    public String getBrowserUnderTest() {
        return browserUnderTest.get() == null ? "" : browserUnderTest.get();
    }

    public void setBrowserVersion(String bv) {
        if (bv.equalsIgnoreCase("jenkinsParameter")) {
            if (System.getProperty("jenkinsBrowserVersion") == null
                    || System.getProperty("jenkinsBrowserVersion") == "null") {
                browserVersion.set("");
            } else {
                browserVersion.set(System.getProperty("jenkinsBrowserVersion").trim());
            }
        } else {
            browserVersion.set(bv);
        }
    }

    public String getBrowserVersion() {
        return browserVersion.get();
    }

    protected void setRunLocation(String location) {
        if (location.equalsIgnoreCase("jenkinsParameter")) {
            runLocation.set(System.getProperty("jenkinsRunLocation".trim()));
        } else {
            runLocation.set(location);
        }
    }

    public String getRunLocation() {
        return runLocation.get() == null ? "" : runLocation.get();
    }

    /*
     * Mobile Specific
     */
    protected void setDeviceID(String deviceId) {
        deviceID.set(deviceId);
    }

    protected void setMobileOSVersion(String mobileVersion) {
        mobileOSVersion.set(mobileVersion);
    }

    public String getRemoteURL() {
        if (getRunLocation().equalsIgnoreCase("sauce")) {
            final String user = DriverProperties.getSaucelabsUsername();
            final String key = DriverProperties.getSaucelabsKey();

            if (StringUtils.isEmpty(user) || StringUtils.isEmpty(key)) {
                throw new WebException("Missing required properties [ chameleon.selenium.hub.saucelabs.username ] or [ chameleon.selenium.hub.saucelabs.key ]. These need to be added in global config properties");
            }

            final String decryptedUser = EncryptDecryptHelper.decrypt(user);
            final String decryptedKey = EncryptDecryptHelper.decrypt(key);

            final SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(decryptedUser, decryptedKey);

            final String sauceLabsURL = "http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub";
            return sauceLabsURL;
        } else if (getRunLocation().equalsIgnoreCase("grid")) {
            final String url = DriverProperties.getWebHubUrl();
            if (StringUtils.isEmpty(url)) {
                throw new WebException("Missing required properties [ chameleon.selenium.hub.web.url ] . These need to be added in global config properties");
            }
            return url;
        } else if (getRunLocation().equalsIgnoreCase("mobile")) {
            final String url = DriverProperties.getMobileHubUrl();
            if (StringUtils.isEmpty(url)) {
                throw new WebException("Missing required properties [ chameleon.selenium.hub.mobile.url ] . These need to be added in global config properties");
            }
            return url;
        } else {
            return "";
        }
    }

    private void driverSetup() {
        if (getRunLocation().equalsIgnoreCase("grid") || getRunLocation().equalsIgnoreCase("sauce")) {
            remoteDriverSetup();
        }
        // Code for running on mobile devices
        else if (getRunLocation().equalsIgnoreCase("mobile")) {
            mobileDriverSetup();
        } else {
            throw new AutomationException(
                    "Parameter for run [Location] was not set to 'Local', 'Grid', 'Sauce', 'Mobile'");
        }

        // Microsoft Edge Browser
        if (!getRunLocation().equalsIgnoreCase("mobile")) {
            DriverManager.getDriver().manage().deleteAllCookies();
            DriverManager.getDriver().manage().window().maximize();
        }
    }

    /**
     * Creates the remote webdriver instance based on browser, browser version
     * OS, and the remote grid URL
     *
     * @author jessica.marshall
     * @date 9/13/2016
     */
    private void remoteDriverSetup() {
        DriverOptionsManager options = new DriverOptionsManager();
        DriverType type = DriverType.fromString(getBrowserUnderTest());

        if (!getBrowserVersion().isEmpty()) {
            // Setting Browser version if desired
            options.setBrowserVersion(type, getBrowserVersion());
        }

        // Setting default Broswer options
        switch (DriverType.fromString(getBrowserUnderTest())) {
            case SAFARI:
                // options.getSafariOptions().useCleanSession(true);
                // options.getSafariOptions().setCapability(SafariOptions.CAPABILITY, options.getSafariOptions());
                break;
            case INTERNETEXPLORER:
                options.getInternetExplorerOptions().ignoreZoomSettings();
                break;
            default:
                break;
        }

        // Operating System
        options.setPlatform(type, getOperatingSystem());
        options.setCapability(type, "name", getTestName());
        // Create the remote web driver
        URL url = null;
        try {
            url = new URL(getRemoteURL());

        } catch (MalformedURLException e) {
            throw new AutomationException("Failed to create Remote WebDriver", e);
        }
        DriverManagerFactory.getManager(type, options).initalizeDriver(url);
        // allows for local files to be uploaded via remote webdriver on grid machines
        DriverManager.getDriver().setFileDetector();
    }

    /**
     * Sets up the driver with capabilities for mobile devices. Uses a remote mobile hub URL
     * Gives user option to either specify the device to test on using deviceID or to give
     * parameters for auto selection of device. If deviceID is null, then will do auto selection using
     * these parameters:
     * operatingSystem -- mobile OS platform, e.g. iOS, Android
     * mobileOSVersion -- Mobile OS version, e.g. 7.1, 4.4
     * browserUnderTest -- Name of mobile web browser to automate. Should be an empty string if automating an app instead
     * mobileAppPath -- The absolute local path or remote http URL to an .ipa or .apk file, or a .zip containing one of these.
     * Leave browserUnderTest blank/null if using this
     *
     * @date 9/28/2016
     * @author jessica.marshall
     */
    private void mobileDriverSetup() {
        final DriverOptionsManager options = new DriverOptionsManager();
        final DriverType type = DriverType.fromString(getOperatingSystem());
        final String mobileHubKey = DriverProperties.getMobileKey();

        // Setting default Broswer options
        switch (type) {
            case IOS_WEB:
                options.getIOSOptions().setCapability(CapabilityType.BROWSER_NAME, MobileBrowserType.SAFARI);
                options.getIOSOptions().setCapability(CapabilityType.VERSION, "Any");
                options.getIOSOptions().setCapability(CapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
                options.getIOSOptions().setCapability("testName", getTestName());
                options.getIOSOptions().setCapability("accessKey", mobileHubKey);
                break;
            case ANDROID_WEB:
                options.getSafariOptions().setCapability(CapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);
                options.getSafariOptions().setCapability(CapabilityType.VERSION, "Any");
                options.getSafariOptions().setCapability(CapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
                options.getIOSOptions().setCapability("testName", getTestName());
                options.getIOSOptions().setCapability("accessKey", mobileHubKey);
                break;
            default:
                break;
        }

        // Operating System
        // Create the remote web driver
        URL url = null;
        try {
            url = new URL(getRemoteURL());

        } catch (MalformedURLException e) {
            throw new AutomationException("Failed to create Remote WebDriver", e);
        }
        DriverManagerFactory.getManager(type, options).initalizeDriver(url);
    }
}
