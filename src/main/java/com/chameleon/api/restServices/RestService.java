package com.chameleon.api.restServices;

import static com.chameleon.api.WebServiceConstants.DEFAULT_REST_TIMEOUT;
import static com.chameleon.utils.TestReporter.logInfo;
import static com.chameleon.utils.TestReporter.logTrace;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.ws.WebServiceException;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;

import com.chameleon.api.restServices.exceptions.RestException;
import com.chameleon.api.soapServices.exceptions.SoapException;
import com.chameleon.utils.io.PropertiesManager;

public class RestService {
    private int timeout = DEFAULT_REST_TIMEOUT;
    private List<BasicHeader> customHeaders = null;
    private boolean acceptAllSSL = false;
    private String environment = null;
    private String propertiesPath = null;
    private String propertyHostName = null;
    private boolean useMatrixParams = false;

    public void setTimeout(final int timeout) {
        this.timeout = timeout;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public void setAcceptAllSSL(final boolean acceptAllSSL) {
        this.acceptAllSSL = acceptAllSSL;
    }

    public boolean getAcceptAllSSL() {
        return this.acceptAllSSL;
    }

    public void setUseMatrixParams(boolean useMatrixParams) {
        this.useMatrixParams = useMatrixParams;
    }

    public void addCustomHeaders(final String header, final String value) {
        if (customHeaders == null) {
            customHeaders = new ArrayList<>();
        } else if (isEmpty(header) || isEmpty(value)) {
            throw new RestException("Header name and value cannot be null: Header: [ %s ] Value: [ %s ]", header, value);
        }

        customHeaders.add(new BasicHeader(header, value));
    }

    public void setPropertiesLocation(final String propertiesPath) {
        this.propertiesPath = propertiesPath;
    }

    public void setEnvironment(final String environment) {
        this.environment = environment;
    }

    public void setPropertyHostName(final String propertyHostName) {
        this.propertyHostName = propertyHostName;
    }

    /**
     * Sends a GET request to a URL
     *
     * @param URL
     *            for the service you are testing
     * @return response in string format
     */
    public RestResponse sendGetRequest(final String url) {
        return sendGetRequest(url, null, null);
    }

    /**
     * Sends a GET request
     *
     * @param url
     *            for the service you are testing
     * @return response in string format
     * @throws ClientProtocolException
     * @throws IOException
     */
    public RestResponse sendGetRequest(final String url, final Map<String, String> headers) {
        return sendGetRequest(url, headers, null);
    }

    /**
     * Sends a GET request
     *
     * @param urlOrPath
     *            for the service you are testing
     * @return response in string format
     * @throws ClientProtocolException
     * @throws IOException
     */
    public RestResponse sendGetRequest(final String urlOrPath, final Map<String, String> headers, final List<NameValuePair> params) {
        logTrace("Entering RestService#sendGetRequest");

        final String url = buildUrl(urlOrPath);

        logInfo("Creating Http GET instance with URL of [ %s ]", url);
        HttpGet request = new HttpGet(url);

        if (params != null) {
            request = new HttpGet(createQueryParamUrl(url, params));
        }

        request.setHeaders(createHeaders(headers));

        final RestResponse response = sendRequest(request);
        logTrace("Exiting RestService#sendGetRequest");
        return response;
    }

    public RestResponse sendPostRequest(final String urlOrPath, final Map<String, String> headers, final List<NameValuePair> params, final String json) {
        logTrace("Entering RestService#sendPostRequest");

        final String url = buildUrl(urlOrPath);

        logInfo("Creating Http POST instance with URL of [ %s ]", url);
        HttpPost httppost = new HttpPost(url);

        if (params != null) {
            httppost = new HttpPost(createQueryParamUrl(url, params));
        }

        if (json != null) {
            logInfo("Adding json %s", json);
            httppost.setEntity(createJsonEntity(json));
        }

        httppost.setHeaders(createHeaders(headers));

        final RestResponse response = sendRequest(httppost);
        logTrace("Exiting RestService#sendPostRequest");
        return response;
    }

    /**
     * Sends a post (update) request, pass in the parameters for the json arguments to update
     *
     * @param url
     *            for the service
     * @param params
     *            arguments to update
     * @return response in string format
     * @throws ClientProtocolException
     * @throws IOException
     */
    public RestResponse sendPostRequest(final String url, final List<NameValuePair> params) {
        return sendPostRequest(url, null, params, null);
    }

    /**
     * Sends a post (update) request, pass in the parameters for the json arguments to update
     *
     * @param url
     *            for the service
     * @param params
     *            arguments to update
     * @return response in string format
     * @throws ClientProtocolException
     * @throws IOException
     */
    public RestResponse sendPostRequest(String url, Map<String, String> headers) {
        return sendPostRequest(url, headers, null, null);
    }

    public RestResponse sendPostRequest(String url, Map<String, String> headers, List<NameValuePair> params) {
        return sendPostRequest(url, headers, params, null);
    }

    public RestResponse sendPostRequest(String url, Map<String, String> headers, String body) {
        return sendPostRequest(url, headers, null, body);
    }

    public RestResponse sendPutRequest(String urlOrPath, Map<String, String> headers, List<NameValuePair> params, String json) {
        logTrace("Entering RestService#sendPutRequest");

        final String url = buildUrl(urlOrPath);

        logInfo("Creating Http PUT instance with URL of [ %s ]", url);
        HttpPut httpPut = new HttpPut(url);

        if (params != null) {
            httpPut = new HttpPut(createQueryParamUrl(url, params));
        }

        if (json != null) {
            logInfo("Adding json %s", json);
            httpPut.setEntity(createJsonEntity(json));
        }

        httpPut.setHeaders(createHeaders(headers));

        RestResponse response = sendRequest(httpPut);
        logTrace("Exiting RestService#sendPutRequest");
        return response;
    }

    public RestResponse sendPutRequest(String url, Map<String, String> headers, String json) {
        return sendPutRequest(url, headers, null, json);
    }

    /**
     * Sends a put (create) request, pass in the parameters for the json arguments to create
     *
     * @param url
     *            for the service
     * @param params
     *            arguments to update
     * @return response in string format
     * @throws ClientProtocolException
     * @throws IOException
     */
    public RestResponse sendPutRequest(String url, Map<String, String> headers, List<NameValuePair> params) {
        return sendPutRequest(url, headers, params, null);
    }

    public RestResponse sendPutRequest(String url, Map<String, String> headers) {
        return sendPutRequest(url, headers, null, null);
    }

    public RestResponse sendPutRequest(String url, List<NameValuePair> params) {
        return sendPutRequest(url, null, params, null);
    }

    public RestResponse sendPutRequest(String url, String json) {
        return sendPutRequest(url, null, json);
    }

    public RestResponse sendPatchRequest(String urlOrPath, Map<String, String> headers, List<NameValuePair> params, String json) {
        logTrace("Entering RestService#sendPatchRequest");

        final String url = buildUrl(urlOrPath);

        logInfo("Creating Http PATCH instance with URL of [ %s ]", url);
        HttpPatch httpPatch = new HttpPatch(url);
        if (params != null) {
            httpPatch = new HttpPatch(createQueryParamUrl(url, params));
        }

        httpPatch.setHeaders(createHeaders(headers));

        if (json != null) {
            logInfo("Adding json %s", json);
            httpPatch.setEntity(createJsonEntity(json));
        }

        RestResponse response = sendRequest(httpPatch);
        logTrace("Exiting RestService#sendPatchRequest");
        return response;
    }

    /**
     * Sends a patch (update) request, pass in the parameters for the json arguments to update
     *
     * @param url
     *            for the service
     * @param params
     *            arguments to update
     * @return response in string format
     * @throws ClientProtocolException
     * @throws IOException
     */
    public RestResponse sendPatchRequest(String url, Map<String, String> headers, List<NameValuePair> params) {
        return sendPatchRequest(url, headers, params, null);
    }

    public RestResponse sendPatchRequest(String url, List<NameValuePair> params, String json) {
        return sendPatchRequest(url, null, params, json);
    }

    /**
     * Sends a patch (update) request, pass in the parameters for the json arguments to update
     *
     * @param url
     *            for the service
     * @param params
     *            arguments to update
     * @return response in string format
     * @throws ClientProtocolException
     * @throws IOException
     */
    public RestResponse sendPatchRequest(String url, List<NameValuePair> params) {
        return sendPatchRequest(url, null, params, null);
    }

    public RestResponse sendPatchRequest(String url, Map<String, String> headers, String json) {
        return sendPatchRequest(url, headers, null, json);
    }

    public RestResponse sendDeleteRequest(String urlOrPath, Map<String, String> headers, List<NameValuePair> params) {
        logTrace("Entering RestService#sendDeleteRequest");

        final String url = buildUrl(urlOrPath);

        logInfo("Creating Http DELETE instance with URL of [ %s ]", url);
        HttpDelete httpDelete = new HttpDelete(url);

        if (params != null) {
            httpDelete = new HttpDelete(createQueryParamUrl(url, params));
        }

        httpDelete.setHeaders(createHeaders(headers));

        RestResponse response = sendRequest(httpDelete);
        logTrace("Exiting RestService#sendDeleteRequest");
        return response;
    }

    /**
     * Sends a delete request. Depends on the service if a response is returned.
     * If no response is returned, will return null *
     *
     * @param url
     *            for the service
     * @return response in string format or null
     * @throws ClientProtocolException
     * @throws IOException
     */

    public RestResponse sendDeleteRequest(String url, Map<String, String> headers) {
        return sendDeleteRequest(url, headers, null);
    }

    public RestResponse sendDeleteRequest(String url) {
        return sendDeleteRequest(url, null, null);
    }

    /**
     * Sends an options request. Options should give what the acceptable methods are for
     * the service (GET, HEAD, PUT, POST, etc). There should be some sort of an ALLOW
     * header that will give you the allowed methods. May or may not be a body to the response,
     * depending on the service.
     *
     * This method will return all the headers and the test should parse through and find the header
     * it needs, that will give the allowed methods, as the naming convention will be different for each service.
     *
     * @param URL
     *            for the service
     * @return returns an array of headers
     * @throws ClientProtocolException
     * @throws IOException
     */
    public Header[] sendOptionsRequest(String urlOrPath) {
        logTrace("Entering RestService#sendOptionsRequest");

        final String url = buildUrl(urlOrPath);

        logInfo("Creating Http OPTIONS instance with URL of [ %s ]", url);
        HttpOptions httpOptions = new HttpOptions(url);
        return sendRequest(httpOptions).getHeaders();
    }

    private RestResponse sendRequest(HttpUriRequest request) {
        logTrace("Entering RestService#sendRequest");
        RestResponse response = null;
        CloseableHttpClient sslHttpClient = null;
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();

        if (acceptAllSSL) {
            sslHttpClient = TrustedSSLContext.buildHttpClient(timeout);
        }

        try (CloseableHttpClient client = (sslHttpClient == null
                ? HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build()
                : sslHttpClient)) {

            logTrace("Sending request");
            StopWatch execution = StopWatch.createStarted();
            HttpResponse httpResponse = client.execute(request);
            execution.stop();
            String executionTime = execution.toString();
            response = new RestResponse(request, httpResponse, executionTime);
        } catch (SocketTimeoutException | SocketException t1) {
            logTrace("Failed to establish connection after [ %s ] seconds: %s", timeout, t1.getMessage());
            throw new RestException("Failed to establish connection after [ %s ] seconds", t1, timeout);
        } catch (IOException e) {
            throw new RestException("Failed to send request to %s", e, request.getURI().toString());
        }

        logTrace("Returning RestResponse to calling method");
        logTrace("Exiting RestService#sendRequest");
        return response;
    }

    private String buildUrl(final String url) {
        if (isNotEmpty(propertiesPath)) {
            if (propertiesPath.contains("%s") && isEmpty(environment)) {
                throw new SoapException("Environment was not set when properties path expects it [ %s ]", propertiesPath);
            }

            final String path = isEmpty(environment) ? propertiesPath : String.format(propertiesPath, environment);
            final Map<String, String> props = PropertiesManager.properties(path);
            final String propertyName = isEmpty(propertyHostName) ? "app.endpoint" : propertyHostName + ".endpoint";

            logTrace("Look for property host name [ %s ] from property file [ %s ]", propertyName, path);
            final String endpoint = props.get(propertyName);
            if (endpoint != null) {
                return endpoint + url;
            }

            if (isEmpty(url)) {
                throw new WebServiceException("No url was set");
            }

        }

        logTrace("Property path not set. Using url [ %s ]", url);
        return url;
    }

    private Header[] createHeaders(Map<String, String> requestHeaders) {
        Header[] headers = null;

        if (requestHeaders != null && !requestHeaders.isEmpty()) {
            headers = new Header[requestHeaders.size()];
            int x = 0;
            for (Entry<String, String> header : requestHeaders.entrySet()) {
                headers[x] = new BasicHeader(header.getKey(), header.getValue());
                x++;
            }
        }

        if (customHeaders != null && !customHeaders.isEmpty()) {

            int start = 0;
            if (headers == null) {
                headers = new Header[customHeaders.size()];
            } else {
                start = headers.length;
                headers = Arrays.copyOf(headers, headers.length + customHeaders.size());
            }

            int x = 0;
            for (BasicHeader header : customHeaders) {
                headers[start + x] = header;
                x++;
            }
        }

        return headers;
    }

    private String createQueryParamUrl(String url, List<NameValuePair> params) {
        String allParams = "";
        String matrixParams = "";
        for (NameValuePair param : params) {
            allParams += "[" + param.getName() + ": " + param.getValue() + "] ";
            matrixParams += ";" + param.getName() + "=" + param.getValue();
        }
        logInfo("Adding Parameters " + allParams);
        if (useMatrixParams) {
            url = url + matrixParams;
        } else {
            url = url + "?" + URLEncodedUtils.format(params, "utf-8");
        }
        logInfo("URL with params [ %s ]", url);
        return url;
    }

    private ByteArrayEntity createJsonEntity(String json) {
        ByteArrayEntity entity = null;
        try {
            entity = new ByteArrayEntity(json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // This literally cannot be reached, but has to be checked anyway
            throw new RestException(e.getMessage(), e);
        }

        return entity;
    }
}