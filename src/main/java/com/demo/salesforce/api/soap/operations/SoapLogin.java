package com.demo.salesforce.api.soap.operations;

import java.io.File;
import java.util.ResourceBundle;

import com.chameleon.utils.Base64Coder;
import com.chameleon.utils.Constants;
import com.chameleon.utils.TestReporter;
import com.chameleon.utils.XMLTools;
import com.demo.salesforce.api.soap.SalesforceSoapService;

public class SoapLogin extends SalesforceSoapService {

	private static String TEST_ENV_URL = "https://login.salesforce.com/services/Soap/c/42.0/0DFf4000000TOm2";
	
	/**
	 * Constructor for the Login operation; imports the LoginRequest XML template
	 * and converts it to an XML document, cleans it up, etc.
	 * 
	 * @author John Martin
	 * @date 04/18/2018
	 */
	public SoapLogin() {
		File loginXML = new File(this.getClass().getResource("/xmls/salesforce/LoginRequest.xml").getPath());
		setOperationName("Login to Salesforce");
        setRequestDocument(XMLTools.makeXMLDocument(loginXML));
        removeComments();
        removeWhiteSpace();
	}
	
	/**
	 * Login to Salesforce API based on environment parameter set
	 * in the Maven pom.xml file updates either locally or through Jenkins.
	 * This environment values will determine which user to use, with the username
	 * controlling which environment is launched/API to authenticate to.
	 * 
	 * In addition, sets the sessionID and serverURL.
	 * 
	 * @author John Martin
	 * @date 04/18/2018
	 */
	public void loginToSalesforceAPI() {
		//Setting the Soap Service URL for login authentication.
		setServiceURL(TEST_ENV_URL);
		
		//Retrieve the resource bundle of all the usernames and passwords.
		ResourceBundle userCredentialRepo = ResourceBundle.getBundle(Constants.USER_CREDENTIALS_PATH);
		
		//Using the retrieved test environment parameter from the Maven pom.xml file,
		//this will determine the prefix for retrieving credentials from the
		//UserCredentials.properties file by reference.
		String userName = userCredentialRepo.getString("SALESFORCE_SOAP_USERNAME");
		String password = Base64Coder.decodeString(userCredentialRepo.getString("SALESFORCE_SOAP_PASSWORD"));
		
		//Login to Salesforce API based on username and reset Soap Service URL.
		loginToSalesforceAPI(userName, password);
		setServiceURL(getServerURL());
	}
	
	/**
	 * Login to Salesforce API based on username and password values.
	 * 
	 * @param username
	 * @param password
	 */
	public void loginToSalesforceAPI(String username, String password) {
		setUsername(username);
        setPassword(password);
        addRequestHeader("SoapAction", "Test");
        sendRequest();
        TestReporter.logAPI(getResponseStatusCode().equals("200"), "Login Operation", this);
        TestReporter.assertTrue(getResponseStatusCode().equals("200"), "SOAP API :: Successful login to Salesforce Sandbox via SOAP API. Response Status Code: [ "+ getResponseStatusCode() +" ].");
	}
	
	
	//------------Getters & Setters: Login Request & Response------------
	
	/**
	 * Sets the value <urn:username> XML node in the Login Request XML file.
	 * 
	 * @author John Martin
	 * @date 04/18/2018
	 * @param value - username value to update in the XML node.
	 */
    public void setUsername(String value) {
        setRequestNodeValueByXPath("/Envelope/Body/login/username", value);
    }

    /**
     * Sets the value <urn:password> XML node in the Login Request XML file.
     * 
     * @author John Martin
     * @date 04/18/2018
     * @param value - password value to update in the XML node.
     */
    public void setPassword(String value) {
        setRequestNodeValueByXPath("/Envelope/Body/login/password", value);
    }

    /**
     * Retrieves the sessionID value from the Login response; this will
     * be used throughout the rest of the session's calls.
     * 
     * @author John Martin
     * @date 04/18/2018
     * @return String - the session ID value returned in the response.
     */
    public String getSessionID() {
        return getResponseNodeValueByXPath("/Envelope/Body/loginResponse/result/sessionId");
    }

    /**
     * Retrieves the serverURL value from the Login response; this will
     * be used throughout the rest of the session's calls.
     * 
     * @author John Martin
     * @date 04/18/2018
     * @return String - the serverURL value returned in the response.
     */
    public String getServerURL() {
        return getResponseNodeValueByXPath("/Envelope/Body/loginResponse/result/serverUrl");
    }
    
}
