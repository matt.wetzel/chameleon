package com.demo.salesforce.api.soap;

import org.apache.commons.lang.StringUtils;

import com.chameleon.api.soapServices.SoapService;
import com.demo.salesforce.api.soap.operations.SoapLogin;

public class SalesforceSoapService extends SoapService {

    private static String serverURL = "";
    protected static String sessionID = null;

    public SalesforceSoapService() {
        setServiceName("Salesforce SOAP Request");
        setServiceURL(serverURL);
    }

    /**
     * Verifies whether an established API sessions exists,
     * if not, it establishes a session/calls login to the API service.
     * 
     * @author John Martin
     */
    protected void verifyAuthentication() {
        if (StringUtils.isEmpty(sessionID)) {
            SoapLogin login = new SoapLogin();
            login.loginToSalesforceAPI();
            serverURL = login.getServerURL();
            setServiceURL(login.getServerURL());
            sessionID = login.getSessionID();
        } else {
            setServiceURL(serverURL);
        }
    }

}
