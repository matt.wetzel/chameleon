package com.demo.salesforce.api.soap.operations;

import java.io.File;

import com.chameleon.utils.TestReporter;
import com.chameleon.utils.XMLTools;
import com.chameleon.utils.exception.XPathNotFoundException;
import com.demo.salesforce.api.soap.SalesforceSoapService;

public class Delete extends SalesforceSoapService {
	
	public Delete() {
		File xml = new File(this.getClass().getResource("/xmls/salesforce/DeleteRequest.xml").getPath());
		setOperationName("Delete Salesforce SObject");
        setRequestDocument(XMLTools.makeXMLDocument(xml));
        removeComments();
        removeWhiteSpace();
        verifyAuthentication();
        addRequestHeader("SoapAction", "Test");
        setSessionID(sessionID);
	}
	
	public void deleteObject(String sObjectID) {
		setSObjectID(sObjectID);
		sendRequest();
        // Log soap response results to the reporter
        TestReporter.logAPI(getResponseStatusCode().equals("200"), "Details regarding the delete request", this);
        // Verify a 200 response comes back
        TestReporter.assertTrue(getResponseStatusCode().equals("200"), "Verify the delete request was successful");
        // Verify no errors were returned
        if (getSuccess().equals("false")) {
            TestReporter.assertTrue(false, "Delete Account request not successful - error message: " + getErrorMessage());
        }
	}

	//------------Getters & Setters: Delete Request & Response------------
	
	public void setSessionID(String value) {
        setRequestNodeValueByXPath("/Envelope/Header/SessionHeader/sessionId", value);
    }
	
	public void setSObjectID(String value) {
        setRequestNodeValueByXPath("/Envelope/Body/delete/ids", value);
    }

    public String getSuccess() {
        return getResponseNodeValueByXPath("/Envelope/Body/deleteResponse/result/success");
    }
    
    public boolean isSuccess() {
    	try {
    		return getSuccess().equals("true");
    	} catch (XPathNotFoundException e) {
    		return false;
    	}
    }

    public String getErrorMessage() {
        return getResponseNodeValueByXPath("/Envelope/Body/deleteResponse/result/errors/message");
    }
}
