package com.demo.etsy.ui;

import org.openqa.selenium.By;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.ExtendedWebDriver;

public class SearchPage {

    private ExtendedWebDriver driver;

    // By locators for elements in the DOM.
    By searchBar = By.id("global-enhancements-search-query");
    By searchButton = By.xpath("//button[@value='Search']");
    By searchResultsLabel = By.xpath("//h2[text()='Search results']");

    // Page Object constructor.
    public SearchPage() {
        driver = DriverManager.getWebDriver();
    }

    // Search for an item via the search bar.
    public boolean search(String searchTerm) {
        // Verify the elements are displayed.
        driver.findTextbox(searchBar).syncVisible();
        driver.findButton(searchButton).syncVisible();

        // Set the search field and click the search button.
        driver.findTextbox(searchBar).set(searchTerm);
        driver.findButton(searchButton).click();

        // Verify the search results label is displayed.
        return driver.findLabel(searchResultsLabel).syncVisible();
    }

}
