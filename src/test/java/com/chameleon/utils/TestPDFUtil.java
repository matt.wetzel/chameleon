package com.chameleon.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.BaseTest;
import com.chameleon.utils.io.PDFUtil;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestPDFUtil extends BaseTest {
    @Feature("Utilities")
    @Story("PDFUtil")
    @Test
    public void testPDFUtilTextSame() {
        PDFUtil util = new PDFUtil();
        util.setCompareMode(PDFUtil.CompareMode.TEXT_MODE);
        Assert.assertTrue(util.compare("/pdf/txt-sample1.pdf", "/pdf/txt-sample1.pdf"));
    }

    @Feature("Utilities")
    @Story("PDFUtil")
    @Test
    public void testPDFUtilTextNotSame() {
        PDFUtil util = new PDFUtil();
        util.setCompareMode(PDFUtil.CompareMode.TEXT_MODE);
        Assert.assertFalse(util.compare("/pdf/txt-sample1.pdf", "/pdf/txt-sample2.pdf"));
    }

    @Feature("Utilities")
    @Story("PDFUtil")
    @Test
    public void testPDFUtilImageNotSame() {
        PDFUtil util = new PDFUtil();
        util.setCompareMode(PDFUtil.CompareMode.VISUAL_MODE);
        Assert.assertFalse(util.compare("/pdf/img-sample1.pdf", "/pdf/img-sample2.pdf"));
    }

    @Feature("Utilities")
    @Story("PDFUtil")
    @Test
    public void testPDFUtilImageSame() {
        PDFUtil util = new PDFUtil();
        util.setCompareMode(PDFUtil.CompareMode.VISUAL_MODE);
        Assert.assertTrue(util.compare("/pdf/img-sample1.pdf", "/pdf/img-sample1.pdf"));
    }

}
