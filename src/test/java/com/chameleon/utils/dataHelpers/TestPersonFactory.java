package com.chameleon.utils.dataHelpers;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.utils.dataHelpers.creditCards.CardType;
import com.chameleon.utils.dataHelpers.creditCards.CreditCard;
import com.chameleon.utils.dataHelpers.creditCards.CreditCardManager;
import com.chameleon.utils.dataHelpers.personFactory.Address;
import com.chameleon.utils.dataHelpers.personFactory.Email;
import com.chameleon.utils.dataHelpers.personFactory.Party;
import com.chameleon.utils.dataHelpers.personFactory.Person;
import com.chameleon.utils.dataHelpers.personFactory.Phone;
import com.chameleon.utils.dataHelpers.personFactory.seeds.FemaleFirstNames;
import com.chameleon.utils.dataHelpers.personFactory.seeds.MaleFirstNames;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestPersonFactory {
    Party party = new Party(1);

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void partyConstructor() {
        Assert.assertNotNull(party);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void partyPrimaryPerson() {
        Assert.assertNotNull(party.primaryPerson());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "partyPrimaryPerson")
    public void partyGetAllPersons() {
        Assert.assertTrue(party.getAllPersons().size() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "partyGetAllPersons")
    public void partyAddPerson() {
        party.addPerson(new Person());
        party.addPerson(new Person());
        party.getAllPersons().get(1).setAge(15);
        party.getAllPersons().get(2).setAge(2);
        Assert.assertNotNull(party.getAllPersons().size() == 3);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "partyAddPerson")
    public void partyNumberOfAdults() {
        Assert.assertTrue(party.numberOfAdults() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "partyAddPerson")
    public void partyNumberOfChildren() {
        Assert.assertTrue(party.numberOfChildren() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "partyAddPerson")
    public void partyNumberOfInfants() {
        Assert.assertTrue(party.numberOfInfants() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void maleFirstNames() {
        Assert.assertNotNull(MaleFirstNames.getFirstName());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void femaleFirstNames() {
        Assert.assertNotNull(FemaleFirstNames.getFirstName());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personAge() {
        party.primaryPerson().setAge(35);
        Assert.assertTrue(party.primaryPerson().getAge() == 35);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personBirthDate() {
        party.primaryPerson().setBirthDate("05-05-2005");
        Assert.assertTrue(party.primaryPerson().getBirthDate().equals("05-05-2005"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personDeceased() {
        party.primaryPerson().setDeceased(false);
        Assert.assertFalse(party.primaryPerson().getDeceased());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personFirstName() {
        party.primaryPerson().setFirstName("Joe");
        Assert.assertTrue(party.primaryPerson().getFirstName().equals("Joe"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personMiddleName() {
        party.primaryPerson().setMiddleName("Schmoe");
        Assert.assertTrue(party.primaryPerson().getMiddleName().equals("Schmoe"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personLastName() {
        party.primaryPerson().setLastName("Wat");
        Assert.assertTrue(party.primaryPerson().getLastName().equals("Wat"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personFullName() {
        party.primaryPerson().setLastName("Wat");
        Assert.assertTrue(party.primaryPerson().getFullName().equals(party.primaryPerson().getFirstName() + " " + party.primaryPerson().getLastName()));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personNickname() {
        party.primaryPerson().setNickname("Joey");
        Assert.assertTrue(party.primaryPerson().getNickname().equals("Joey"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personPassword() {
        party.primaryPerson().setPassword("secret");
        Assert.assertTrue(party.primaryPerson().getPassword().equals("secret"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personPrimary() {
        party.primaryPerson().setPrimary(true);
        Assert.assertTrue(party.primaryPerson().isPrimary());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personSsn() {
        party.primaryPerson().setSsn("0123456");
        Assert.assertTrue(party.primaryPerson().getSsn().equals("0123456"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personSuffix() {
        party.primaryPerson().setSuffix("Jr.");
        Assert.assertTrue(party.primaryPerson().getSuffix().equals("Jr."));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personTitle() {
        party.primaryPerson().setTitle("Mstr.");
        Assert.assertTrue(party.primaryPerson().getTitle().equals("Mstr."));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personUsername() {
        party.primaryPerson().setUsername("Joe.Wat");
        Assert.assertTrue(party.primaryPerson().getUsername().equals("Joe.Wat"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personPrimaryAddress() {
        Assert.assertNotNull(party.primaryPerson().primaryAddress());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personPrimaryPhone() {
        Assert.assertNotNull(party.primaryPerson().primaryPhone());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personPrimaryEmail() {
        Assert.assertNotNull(party.primaryPerson().primaryEmail());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personAllAddress() {
        Assert.assertTrue(party.primaryPerson().getAllAddresses().size() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personAllPhone() {
        Assert.assertTrue(party.primaryPerson().getAllPhones().size() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personAllEmail() {
        Assert.assertTrue(party.primaryPerson().getAllEmails().size() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAllAddress")
    public void personAddAddressDefault() {
        party.primaryPerson().addAddress();
        Assert.assertNotNull(party.primaryPerson().getAllAddresses().size() == 2);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressDefault")
    public void personAddAddressConstructor() {
        Address address = new Address();
        party.primaryPerson().addAddress(address);
        Assert.assertNotNull(party.primaryPerson().getAllAddresses().size() == 3);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAllPhone")
    public void personAddPhoneDefault() {
        party.primaryPerson().addPhone();
        Assert.assertNotNull(party.primaryPerson().getAllPhones().size() == 2);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddPhoneDefault")
    public void personAddPhoneConstructor() {
        Phone phone = new Phone();
        party.primaryPerson().addPhone(phone);
        Assert.assertNotNull(party.primaryPerson().getAllPhones().size() == 3);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAllEmail")
    public void personAddEmailDefault() {
        party.primaryPerson().addEmail();
        Assert.assertNotNull(party.primaryPerson().getAllPhones().size() == 2);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddEmailDefault")
    public void personAddEmailConstructor() {
        Email email = new Email();
        party.primaryPerson().addEmail(email);
        Assert.assertNotNull(party.primaryPerson().getAllEmails().size() == 3);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressAddress1() {
        party.primaryPerson().primaryAddress().setAddress1("123 Test Lane");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getAddress1().equals("123 Test Lane"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressAddress2() {
        party.primaryPerson().primaryAddress().setAddress2("Blah");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getAddress2().equals("Blah"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressCity() {
        party.primaryPerson().primaryAddress().setCity("Greensboro");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getCity().equals("Greensboro"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressCountry() {
        party.primaryPerson().primaryAddress().setCountry("United States");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getCountry().equals("United States"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressCountryAbbv() {
        party.primaryPerson().primaryAddress().setCountryAbbv("US");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getCountryAbbv().equals("US"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressLocatorId() {
        party.primaryPerson().primaryAddress().setLocatorId("1234");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getLocatorId().equals("1234"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")
    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressPrimary() {
        party.primaryPerson().primaryAddress().setPrimary(true);
        Assert.assertTrue(party.primaryPerson().primaryAddress().isPrimary());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressState() {
        party.primaryPerson().primaryAddress().setState("North Carolina");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getState().equals("North Carolina"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressStateAbbv() {
        party.primaryPerson().primaryAddress().setStateAbbv("NC");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getStateAbbv().equals("NC"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "addressAddress1")
    public void addressStreetName() {
        party.primaryPerson().primaryAddress().setStreetName("Telsa Lane");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getStreetName().equals("Telsa Lane"));
        Assert.assertTrue(party.primaryPerson().primaryAddress().getAddress1().equals("123 Telsa Lane"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "addressStreetName")
    public void addressStreetNumber() {
        party.primaryPerson().primaryAddress().setStreetNumber("543");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getStreetNumber().equals("543"));
        Assert.assertTrue(party.primaryPerson().primaryAddress().getAddress1().equals("543 Telsa Lane"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressType() {
        party.primaryPerson().primaryAddress().setType("Business");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getType().equals("Business"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddAddressConstructor")
    public void addressZipCode() {
        party.primaryPerson().primaryAddress().setZipCode("27409");
        Assert.assertTrue(party.primaryPerson().primaryAddress().getZipCode().equals("27409"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddPhoneConstructor")
    public void phoneType() {
        party.primaryPerson().primaryPhone().setType("Business");
        Assert.assertTrue(party.primaryPerson().primaryPhone().getType().equals("Business"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddPhoneConstructor")
    public void phoneCountry() {
        party.primaryPerson().primaryPhone().setCountry("Mexico");
        Assert.assertTrue(party.primaryPerson().primaryPhone().getCountry().equals("Mexico"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddPhoneConstructor")
    public void phoneLocatorId() {
        party.primaryPerson().primaryPhone().setLocatorId("154");
        Assert.assertTrue(party.primaryPerson().primaryPhone().getLocatorId().equals("154"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")
    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddPhoneConstructor")
    public void phoneNumber() {
        party.primaryPerson().primaryPhone().setNumber("1234567890");
        Assert.assertTrue(party.primaryPerson().primaryPhone().getNumber().equals("1234567890"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "phoneNumber")
    public void phoneFormattedNumber() {
        Assert.assertTrue(party.primaryPerson().primaryPhone().getFormattedNumber().equals("123-456-7890"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "phoneNumber")
    public void phoneFormattedNumberCustomFormat() {
        Assert.assertTrue(party.primaryPerson().primaryPhone().getFormattedNumber("(###) ###-####").equals("(123) 456-7890"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddPhoneConstructor")
    public void phonePrimary() {
        party.primaryPerson().primaryPhone().setPrimary(true);
        Assert.assertTrue(party.primaryPerson().primaryPhone().isPrimary());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddEmailConstructor")
    public void emailPrimary() {
        party.primaryPerson().primaryEmail().setPrimary(true);
        Assert.assertTrue(party.primaryPerson().primaryEmail().isPrimary());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddEmailConstructor")
    public void emailType() {
        party.primaryPerson().primaryEmail().setType("Business");
        Assert.assertTrue(party.primaryPerson().primaryEmail().getType().equals("Business"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddEmailConstructor")
    public void emailCountry() {
        party.primaryPerson().primaryEmail().setCountry("Mexico");
        Assert.assertTrue(party.primaryPerson().primaryEmail().getCountry().equals("Mexico"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddEmailConstructor")
    public void emailAddress() {
        party.primaryPerson().primaryEmail().setEmail("test@automation.com");
        Assert.assertTrue(party.primaryPerson().primaryEmail().getEmail().equals("test@automation.com"));
    }

    @Feature("Utilities")
    @Story("PersonFactory")
    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddEmailConstructor")
    public void emailOptIn() {
        party.primaryPerson().primaryEmail().setOptIn(true);
        Assert.assertTrue(party.primaryPerson().primaryEmail().isOptIn());
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" })
    public void personAllCreditCards() {
        Assert.assertTrue(party.primaryPerson().getAllCreditCards().size() == 1);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAllCreditCards")
    public void personAddCreditCardDefault() {
        party.primaryPerson().addCreditCard();
        Assert.assertTrue(party.primaryPerson().getAllCreditCards().size() == 2);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddCreditCardDefault")
    public void personAddCreditCardConstructor() {
        CreditCard creditcard = CreditCardManager.getCreditCardByType("AMEX");
        party.primaryPerson().addCreditCard(creditcard);
        Assert.assertTrue(party.primaryPerson().getAllCreditCards().size() == 3);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddCreditCardConstructor")
    public void personAddCreditCardConstructorEnum() {
        CreditCard creditcard = CreditCardManager.getCreditCardByType(CardType.DISCOVER);
        party.primaryPerson().addCreditCard(creditcard);
        Assert.assertTrue(party.primaryPerson().getAllCreditCards().size() == 4);
    }

    @Feature("Utilities")
    @Story("PersonFactory")

    @Test(groups = { "regression", "utils", "Party" }, dependsOnMethods = "personAddCreditCardConstructor")
    public void personCreditCardPrimary() {
        Assert.assertNotNull(party.primaryPerson().primaryCreditCard());
        Assert.assertTrue(party.primaryPerson().primaryCreditCard().getCardType().equals(CardType.VISA));
    }
}
