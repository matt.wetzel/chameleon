package com.chameleon.api.soapServices;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.api.APIBaseTest;
import com.chameleon.api.soapServices.SoapServiceCommands;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestSoapServiceCommands extends APIBaseTest {

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void addAttribute() {
        Assert.assertTrue(SoapServiceCommands.addAttribute("blah").equals("fx:addattribute;blah"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void addNode() {
        Assert.assertTrue(SoapServiceCommands.addNode("blah").equals("fx:addnode;blah"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void addNodes() {
        Assert.assertTrue(SoapServiceCommands.addNodes("blah/blah2/blah3").equals("fx:addnodes;blah/blah2/blah3"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void addNamespaceNode() {
        Assert.assertTrue(SoapServiceCommands.addNamespace("blah").equals("fx:addnamespace;blah"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getDateDefault() {
        Assert.assertTrue(SoapServiceCommands.getDate().equals("fx:getdate;0"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getDateSpecific() {
        Assert.assertTrue(SoapServiceCommands.getDate("3").equals("fx:getdate;3"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getDateTimeDefault() {
        Assert.assertTrue(SoapServiceCommands.getDateTime().equals("fx:getdatetime;0"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getDateTimeSpecific() {
        Assert.assertTrue(SoapServiceCommands.getDateTime("5").equals("fx:getdatetime;5"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getRandomAlphaNumericDefault() {
        Assert.assertTrue(SoapServiceCommands.getRandomAlphaNumeric().matches("fx:randomalphanumeric;[3-9]"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getRandomAlphaNumericSpecific() {
        Assert.assertTrue(SoapServiceCommands.getRandomAlphaNumeric("8").equals("fx:randomalphanumeric;8"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getRandomNumberDefault() {
        Assert.assertTrue(SoapServiceCommands.getRandomNumber().matches("fx:randomnumber;[3-9]"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getRandomNumberSpecific() {
        Assert.assertTrue(SoapServiceCommands.getRandomNumber("8").equals("fx:randomnumber;8"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getRandomStringDefault() {
        Assert.assertTrue(SoapServiceCommands.getRandomString().matches("fx:randomstring;[3-9]"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void getRandomStringSpecific() {
        Assert.assertTrue(SoapServiceCommands.getRandomString("8").equals("fx:randomstring;8"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void removeAttribute() {
        Assert.assertTrue(SoapServiceCommands.removeAttribute().equals("fx:removeattribute"));
    }

    @Feature("API")
    @Story("SoapServiceCommands")
    
    @Test(groups = { "regression", "smoke" })
    public void removeNode() {
        Assert.assertTrue(SoapServiceCommands.removeNode().equals("fx:removenode"));
    }

}
