package com.chameleon.selenium.web.webelements;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.DriverType;
import com.chameleon.selenium.elements.Link;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebLink;
import com.chameleon.utils.Sleeper;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestLink extends WebBaseTest {
    private ExtendedWebDriver driver;

    @BeforeClass(groups = { "regression", "interfaces", "link", "dev" })
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/link.html");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        DriverManager.setDriver(driver);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Link")

    @Test(groups = { "regression", "interfaces", "link" })
    public void constructorWithElement() {
        driver = testStart("TestLink");
        Assert.assertNotNull((new WebLink(driver, By.xpath("//a[@href='testLinks.html']"))));
        Assert.assertNotNull((new WebLink(driver, By.xpath("//a[@href='testLinks.html']"))));
    }

    @Feature("Element Interfaces")
    @Story("Link")

    @Test(groups = { "regression", "interfaces", "link" }, dependsOnMethods = "constructorWithElement")
    public void click() {
        Link link = driver.findLink(By.xpath("//a[@href='testLinks.html']"));
        link.click();
        Assert.assertTrue(driver.findElement(By.xpath("//a[@href='link.html']")).syncVisible());
    }

    @Feature("Element Interfaces")
    @Story("Link")

    @Test(groups = { "regression", "interfaces", "link" }, dependsOnMethods = "click")
    public void clickNegative() {

        if (DriverType.INTERNETEXPLORER.equals(driver.getDriverType()) || getBrowserUnderTest().isEmpty()) {
            throw new SkipException("Test not valid for " + driver.getDriverType());
        }
        Link link = driver.findLink(By.xpath("//a[@href='hiddenLink.html']"));
        boolean valid = false;
        try {
            link.click();
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }

    @Feature("Element Interfaces")
    @Story("Link")

    @Test(groups = { "regression", "interfaces", "link" }, dependsOnMethods = "click")
    public void jsClick() {
        DriverManager.setDriver(driver);
        WebLink link = driver.findLink(By.xpath("//a[@href='link.html']"));
        link.jsClick();
        Assert.assertTrue(driver.findLink(By.xpath("//a[@href='testLinks.html']")).syncVisible());
    }

    @Feature("Element Interfaces")
    @Story("Link")

    // @Test(groups = { "regression", "interfaces", "link" }, dependsOnMethods = "jsClick")
    public void jsClickNegative() {
        if (driver.getDriverCapability().browserName().contains("explorer")) {
            throw new SkipException("Test not valid for Internet Explorer");
        }
        WebLink link = driver.findLink(By.xpath("//a[@href='hiddenLink.html']"));
        Sleeper.sleep(1);
        driver.findLink(By.xpath("//a[@href='testLinks.html']")).click();
        boolean valid = false;
        try {
            link.jsClick();
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }

    @Feature("Element Interfaces")
    @Story("Link")

    @Test(groups = { "regression", "interfaces", "link" }, dependsOnMethods = "jsClick")
    public void getURL() {
        Link link = driver.findLink(By.xpath("//a[@href='testLinks.html']"));
        Assert.assertTrue(link.getURL().contains("testLinks.html"));
    }
}
