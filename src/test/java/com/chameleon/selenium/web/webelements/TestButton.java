package com.chameleon.selenium.web.webelements;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.elements.Button;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebButton;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestButton extends WebBaseTest {
    private ExtendedWebDriver driver;

    @BeforeClass(groups = { "regression", "interfaces", "button", "dev" })
    public void setup() {
        setApplicationUnderTest("Test App");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/button.html");
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Button")

    @Test(groups = { "regression", "interfaces", "button" })
    public void click() {
        driver = testStart("TestButton");
        Button button = driver.findButton(By.id("click"));
        button.click();
        Assert.assertTrue(driver.findElement(By.id("testClick")).getText().equals("Successful"));
    }

    @Feature("Element Interfaces")
    @Story("Button")

    @Test(groups = { "regression", "interfaces", "button" }, dependsOnMethods = "click")
    public void jsClick() {
        DriverManager.setDriver(driver);
        WebButton button = driver.findButton(By.id("jsClick"));
        button.jsClick();
        Assert.assertTrue(driver.findButton(By.id("testJsClick")).getText().equals("Successful"));
    }
}
