package com.chameleon.selenium.windows;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.utils.TestReporter;
import com.demo.windows.dragDrop.DragDropPage;

public class LegacyApp extends WindowsBaseTest {
    private final DragDropPage page = new DragDropPage();
    private ExtendedWindowsDriver driver;

    @BeforeClass
    @Parameters({ "applicationName" })
    public void appSetup(String applicationName) {
        setApplicationUnderTest(applicationName);
        driver = testStart(applicationName);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        DriverManager.setDriver(driver);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Test(priority = 1)
    public void dragTitleBox() {
        final String text = page.getTitleBoxText();
        page.dragTitleBoxText();
        TestReporter.assertTrue(page.validateTextInDropList(text), "Validate text [ " + text + " ] was transferred");
    }

    @Test(priority = 2)
    public void dragSpinnerBox() {
        final String text = page.getSpinnerText();
        page.dragSpinnerText();
        TestReporter.assertTrue(page.validateTextInDropList(text), "Validate text [ " + text + " ] was transferred");
    }

    @Test(priority = 3)
    public void dragSpinnerBoxAfterIncrement() {
        page.incrementSpinner();
        final String text = page.getSpinnerText();
        page.dragSpinnerText();
        TestReporter.assertTrue(page.validateTextInDropList(text), "Validate text [ " + text + " ] was transferred");
    }

    @Test(priority = 4)
    public void dragSpinnerBoxAfterDecrement() {
        page.decrementSpinner();
        page.decrementSpinner();
        final String text = page.getSpinnerText();
        page.dragSpinnerText();
        TestReporter.assertTrue(page.validateTextInDropList(text), "Validate text [ " + text + " ] was transferred");
    }
}
