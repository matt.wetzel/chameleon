package com.chameleon.selenium.windows;

import org.testng.annotations.Test;

import com.chameleon.utils.TestReporter;
import com.demo.windows.calculator.CalculatorPage;

public class UWPApp extends WindowsBaseTest {
    private CalculatorPage calculatorPage = new CalculatorPage();
    private final static String CALCULATOR_APP = "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App";

    @Test
    public void simpleCalculator() {
        testStart("Calc");
        calculatorPage.selectMenuOptionIfNotLoaded("Standard");
        calculatorPage.add("1", "2");

        TestReporter.assertTrue("3".equals(calculatorPage.getResult()), "Validate Calculator results is 3 as expected");
        TestReporter.logScreenshot("WinAppTest");

    }

    @Test
    public void simpleCalculatorManyNumbers() {
        setApplicationUnderTest(CALCULATOR_APP);
        testStart("Calc");
        calculatorPage.selectMenuOptionIfNotLoaded("Standard");

        calculatorPage.add("1", "2", "3", "4", "5");

        TestReporter.assertTrue("15".equals(calculatorPage.getResult()), "Validate Calculator results is 3 as expected");
        TestReporter.logScreenshot("WinAppTest");

    }

    @Test
    public void simpleCalculatorMenu() {
        setApplicationUnderTest(CALCULATOR_APP);
        testStart("Calc");
        calculatorPage.selectMenuOption("Date Calculation");
        calculatorPage.differenceBetweenDates();
    }
}
