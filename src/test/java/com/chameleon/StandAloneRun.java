package com.chameleon;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.utils.io.PDFUtil;

public class StandAloneRun {

    @Test
    public void test() {
        // TestReporter.setDebugLevel(3);
        PDFUtil util = new PDFUtil();
        util.setCompareMode(PDFUtil.CompareMode.TEXT_MODE);

        if (false == util.compare("/pdf/txt-sample1.pdf", "/pdf/txt-sample2.pdf")) {
            util.setCompareMode(PDFUtil.CompareMode.VISUAL_MODE);
            Assert.assertTrue(util.compare("/pdf/txt-sample1.pdf", "/pdf/txt-sample2.pdf"));
        }

    }

}
