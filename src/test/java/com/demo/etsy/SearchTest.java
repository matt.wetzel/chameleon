package com.demo.etsy;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.utils.TestReporter;
import com.demo.etsy.ui.HomePage;
import com.demo.etsy.ui.SearchPage;

public class SearchTest extends WebBaseTest {

    @DataProvider(name = "searchTerms", parallel = true)
    public Object[][] searchTerms() {
        return new Object[][] { { "handbag" }, { "blanket" }, { "jewelry" } };
    }

    @Test(dataProvider = "searchTerms", groups = { "demo" })
    public void searchForItems(String searchTerm) {
        setEnvironment(null);
        testStart("Search For Items");

        HomePage homePage = new HomePage();
        TestReporter.assertTrue(homePage.verifyHomepageIsDisplayed(), "The Etsy homepage is successfully displayed.");

        SearchPage searchPage = new SearchPage();
        TestReporter.assertTrue(searchPage.search(searchTerm), "Successfully searched for the following item: [ " + searchTerm + " ].");
    }

}
